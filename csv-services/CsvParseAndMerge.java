import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class OutputCsvFormat {
    String idEN; // ID of English sentences
    String textEN; // Text of English sentences
    String audioUrl; // audio url of English sentences
    String idVN; // ID of Vietnamese translation
    String textVN; // Text of Vietnamese translation
}

public class CsvParseAndMerge {

    // Cache -> quy hoach dong
    private final static Map<String,String> cacheEN = new HashMap();
    private final static Map<String,String> cacheVN = new HashMap();
    private final static Map<String,OutputCsvFormat> result = new HashMap<>(); // Key = enId;

    /*
     * Author: DinhNKP
     * Chay voi flag: -Xmx4G
     */
    private static void readCsvFile() {

        try (
            BufferedReader brSentences = new BufferedReader(new FileReader("csv/sentences.csv"));
            BufferedReader brLinks = new BufferedReader(new FileReader("csv/links.csv"));
            BufferedReader brAudio = new BufferedReader(new FileReader("csv/sentences_with_audio.csv"));
            FileWriter fileWriter = new FileWriter("english_vietnamese.csv");
        ){ // auto close after try catch

            // Integer limit = 1000; // Prevent heap out of memory # Another solution is to run java with flags  -Xmx4G
            String line;


            // SCAN sentences.csv
            while ((line = brSentences.readLine()) != null ) {
                String[] values = line.split("\t");

                if (values.length != 3) { // id lang text
                    continue;
                }
                switch (values[1]) {
                    case "eng":
                        cacheEN.put(values[0],values[2]);
                        break;
                    case "vie":
                        cacheVN.put(values[0],values[2]);
                        break;
                }
            }
            System.out.println("CacheEN before: " + cacheEN.size());
            System.out.println("CacheVN before: " + cacheVN.size());


            // SCAN links.csv
            while ((line = brLinks.readLine()) != null) {
                String[] values = line.split("\t");
                if (values.length != 2) { // id idTranslate
                    continue;
                }

                String enTextMain = cacheEN.get(values[0]);
                String vnText = cacheVN.get(values[1]);

                String vnTextMain = cacheVN.get(values[0]);
                String enText = cacheEN.get(values[1]);


                if (enTextMain != null && vnText != null){
                    OutputCsvFormat output = new OutputCsvFormat();

                    output.idEN = values[0];
                    output.textEN = enTextMain;
                    output.idVN = values[1];
                    output.textVN = vnText;

                    result.putIfAbsent(values[0],output);
                } else if (vnTextMain != null && enText != null){

                    OutputCsvFormat output = new OutputCsvFormat();

                    output.idEN = values[1];
                    output.textEN = enText;
                    output.idVN = values[0];
                    output.textVN = vnTextMain;

                    result.putIfAbsent(values[1],output);
                }

            }
            System.out.println("CacheEN after: " + cacheEN.size());
            System.out.println("CacheVN after: " + cacheVN.size());

            // Prevent heap out of memory
            cacheEN.clear();
            cacheVN.clear();
            

            // SCAN sentences_with_audio.csv
            while ((line = brAudio.readLine()) != null) {
                String[] values = line.split("\t");
                if (values.length != 5){ // id id username license audioUrl
                    continue;
                }
                // fileWriter.write(values[0]+"\t"+values[1]+"\t"+values[2]+"\t"+values[3]+"\t"+values[4]+"\n");
                OutputCsvFormat temp = result.get(values[0]);
                if (temp != null) {
                    temp.audioUrl = values[4].contains("manythings.org/tatoeba")? "https://audio.tatoeba.org/sentences/eng/"+temp.idEN+".mp3": null;
                }
                OutputCsvFormat temp1 = result.get(values[1]);
                if (temp1 != null) {
                    temp1.audioUrl = values[4].contains("manythings.org/tatoeba")? "https://audio.tatoeba.org/sentences/eng/"+temp1.idEN+".mp3": null;
                }
            }
            
            // Write
            for (Map.Entry<String,OutputCsvFormat> entry : result.entrySet()){
                OutputCsvFormat val = entry.getValue();
                String str = val.idEN + "\t" + val.textEN + "\t" + val.audioUrl + "\t" + val.idVN + "\t" + val.textVN + "\n";
                fileWriter.write(str);
            }


        } catch (IOException ex) {
            ex.printStackTrace();
        }

        System.out.println("End");
    }


    public static void main(String[] args){
        readCsvFile();
    };
}