package com.apiservices.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.apiservices.springboot.model.Translation;
import com.apiservices.springboot.repository.TranslationRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;


@RestController
@RequestMapping("/api")
public class TranslationController {

    @Autowired
    private TranslationRepository translationRepository;

    private static final Gson gson = new Gson();

    @GetMapping("/translations")
    @ResponseBody
    public ResponseEntity<List<Translation>> getAll(
        @RequestParam(name="page_number",defaultValue = "0") int page,
        @RequestParam(name="page_size",defaultValue = "10") int size 
    ) {
        Pageable pageable = PageRequest.of(page,size,Sort.by("id"));
        Page<Translation> translations = translationRepository.findAll(pageable);
        return ResponseEntity.ok(translations.getContent());
    }
}
