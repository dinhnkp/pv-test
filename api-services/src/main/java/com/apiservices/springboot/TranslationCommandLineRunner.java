package com.apiservices.springboot;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.apiservices.springboot.model.Translation;
import com.apiservices.springboot.repository.TranslationRepository;

@Component
public class TranslationCommandLineRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineRunner.class);
    private static final Integer LIMIT = 1000;

    @Autowired
    private TranslationRepository repository;

    @Override
    public void run(String... args) {

        List<Translation> translations = new ArrayList(); 
        
        String line;
        try (BufferedReader brTranslate = new BufferedReader(new FileReader("english_vietnamese.csv")); ){

            while ((line = brTranslate.readLine()) != null) {
                String[] values = line.split("\t");
                if (values.length != 5) { // idEN textEN audioUrl idVN textVN
                    continue;
                }
                
                Translation temp = new Translation();
                temp.setId(Long.valueOf(values[0]));
                temp.setTextEN(values[1]);
                temp.setAudioUrl(values[2] != "null"? values[2]:"");
                temp.setIdVN(Long.valueOf(values[3]));
                temp.setTextVN(values[4]);

                translations.add(temp);
                if (translations.size() > LIMIT) {
                    repository.saveAll(translations);
                    translations.clear();
                }
            }
            if (translations.size() != 0) {
                repository.saveAll(translations);
            }
        } catch (IOException ex) {
            LOGGER.error("Failed", ex);
        }
        LOGGER.info("Finished import data!");
    }
}
