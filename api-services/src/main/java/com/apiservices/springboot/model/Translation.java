package com.apiservices.springboot.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Translation {

    @Id
    // @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id; // EN ID

    @Column(columnDefinition = "TEXT")
    private String textEN;

    @Column(columnDefinition = "TEXT")
    private String audioUrl;

    private Long idVN;

    @Column(columnDefinition = "TEXT")
    private String textVN;

    public Translation() {
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getId() {
        return id;
    }

    public void setTextEN(String textEN) {
        this.textEN = textEN;
    }
    public String getTextEN() {
        return this.textEN;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }
    public String getAudioUrl() {
        return this.audioUrl;
    }

    public void setIdVN(Long idVN) {
        this.idVN = idVN;
    }
    public Long getIdVN() {
        return this.idVN;
    }

    public void setTextVN(String textVN) {
        this.textVN = textVN;
    }
    public String getTextVN() {
        return this.textVN;
    }
}
