package com.apiservices.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apiservices.springboot.model.Translation;

public interface TranslationRepository extends JpaRepository<Translation, Long> {
}
